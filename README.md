
## About Cloud App

It is a simple service to create a number of temporary users and store it in the cache (Redis).

**Requirements Table:**

| PHP 8.2 | Mysql 8 | Redis |  
|---------|---------|-------|  



1. Run `git clone https://gitlab.com/mimshe/simple-app.git`
2. Run `cd ./simple-app`
3. Run `cp .env.example .env` and configure mysql & redis
4. Run `composer install`
5. Run `php artisan key:generate`
6. Create database tables with this command `php artisan migrate`

7. Call this api:  
   http://yourhost/api/users/sync

8. Example api response
  ```json
  {
  "users": [
    {
      "name": "Name McKenzie I",
      "email": "eugene13@example.org",
      "email_verified_at": "2022-12-07T08:53:43.000000Z",
      "updated_at": "2022-12-07T08:53:43.000000Z",
      "created_at": "2022-12-07T08:53:43.000000Z",
      "id": 16
    },
    {
      "name": "Prof. Elisha Abbott",
      "email": "sigmund.daugherty@example.net",
      "email_verified_at": "2022-12-07T08:53:43.000000Z",
      "updated_at": "2022-12-07T08:53:43.000000Z",
      "created_at": "2022-12-07T08:53:43.000000Z",
      "id": 17
    },
    {
      "name": "Randall Schaefer",
      "email": "akeem.dibbert@example.com",
      "email_verified_at": "2022-12-07T08:53:43.000000Z",
      "updated_at": "2022-12-07T08:53:43.000000Z",
      "created_at": "2022-12-07T08:53:43.000000Z",
      "id": 18
    },
    {
      "name": "Mrs. Jacinthe Heidenreich V",
      "email": "vilma.greenholt@example.net",
      "email_verified_at": "2022-12-07T08:53:43.000000Z",
      "updated_at": "2022-12-07T08:53:43.000000Z",
      "created_at": "2022-12-07T08:53:43.000000Z",
      "id": 19
    },
    {
      "name": "Jean Franecki",
      "email": "marquardt.victoria@example.com",
      "email_verified_at": "2022-12-07T08:53:43.000000Z",
      "updated_at": "2022-12-07T08:53:43.000000Z",
      "created_at": "2022-12-07T08:53:43.000000Z",
      "id": 20
    }
  ]
}
  ``` 
