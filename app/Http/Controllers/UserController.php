<?php

namespace App\Http\Controllers;


use App\Http\Services\SyncUserService;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{
    public function sync(SyncUserService $syncUserService): JsonResponse
    {
        return response()->json(['users' => $syncUserService->sync()]);
    }
}