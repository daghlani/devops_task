#FROM php:8.2-fpm as build
FROM php:8.2-fpm as build
# Install requirements extensions
RUN docker-php-ext-install pdo pdo_mysql sockets
# Install OS requirements
RUN apt-get update && apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip

# Install composer
COPY --from=composer:2.4.4 /usr/bin/composer /usr/bin/composer
WORKDIR /app
COPY . .

# Create a APP_KEY
RUN php artisan key:generate
